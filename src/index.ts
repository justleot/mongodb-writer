import { Logger } from "./core/logger";
import { Mutex } from "async-mutex";
import { Queue } from "./core/queue";
import { Sender } from "./core/sender";

async function main() {
  Logger.info("Application initialization...");

  const mutex = new Mutex();
  const queue = new Queue();
  const sender = new Sender();

  Logger.info("Successfully launched application.");

  process.on("SIGTERM", () => {
    sender.completed = true;
  });

  setTimeout(() => {}, 5 * 60 * 1000);

  await queue.connect();
  await sender.connect();

  const handler = async (message) => {
    const release = await mutex.acquire();

    try {
      await sender.send(JSON.parse(message.content), "prod", "parser.log");
      await queue.channel.ack(message);
    } catch (cause) {
      // @todo описать ошибки
    } finally {
      await release();
    }
  };

  await queue.listen("aggregator.raw.log", handler);
}

main();
