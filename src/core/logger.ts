import tracer from "tracer";

export const Logger = tracer.colorConsole({
  format: "[{{timestamp}}] [{{title}}] {{message}} (in {{file}}:{{line}})",
  dateformat: "yyyy-dd-mm HH:MM:ss",
  preprocess: (data) => {
    data.title = data.title.toUpperCase();
  },
});
