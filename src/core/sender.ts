import config from "../../settings/config.json";
import { Logger } from "./logger";
import { MongoClient } from "mongodb";

export class Sender {
  protected _client: MongoClient;
  protected _enabled: boolean;
  public completed: boolean;
  private readonly settings: {
    url: string;
  };

  constructor() {
    try {
      let { endpoint } = config?.mongoDB;

      const url = new URL(endpoint);
      if (url.searchParams.get("tlsCAFile")) {
        url.searchParams.set("tlsCAFile", "./settings/YandexCA.crt");
      }

      this.completed = false;

      this.settings = {
        url: url.toString(),
      };
    } catch (cause) {
      throw new Error(
        "An error occurred while initializing the sender. " +
          "Parameter mongoDB.endpoint not set in config."
      );
    }
  }

  async connect() {
    if (this.completed) {
      process.exit(0);
    }

    this._client = new MongoClient(this.settings.url);
    this._enabled = false;

    while (true) {
      try {
        Logger.info("Connecting to MongoDB...");
        await this._client.connect();
        Logger.info("Successfully connected to MongoDB.");
        this._enabled = true;
      } catch (cause) {
        Logger.error("Unable to connect to MongoDB. Retry after 5 seconds.");
        setTimeout(() => {}, 5_000);
      }

      if (this._enabled) {
        break;
      }
    }
  }

  async send(data: any, dbName: string, collectionName: string) {
    if (this.completed) {
      process.exit(0);
    }

    while (true) {
      try {
        const db = await this._client.db(dbName);
        const collection = await db.collection(collectionName);
        await collection
          .insertOne(data)
          .then(() =>
            Logger.info(`Successfully send to MongoDB, id: ${data._id}.`)
          );

        break;
      } catch (cause) {
        Logger.error("Unable to writing to MongoDB. Retry after 5 seconds.");
        setTimeout(() => {}, 5_000);
      }
    }
  }
}
