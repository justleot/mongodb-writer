import config from "../../settings/config.json";
import { Logger } from "./logger";
import { connect } from "amqplib";

export class Queue {
  public channel;
  private readonly settings: {
    url: string;
  };

  constructor() {
    try {
      const { endpoint } = config?.queue;

      this.settings = {
        url: endpoint,
      };
    } catch (cause) {
      throw new Error(
        "An error occurred while initializing the queue. " +
          "Parameter queue.endpoint not set in config."
      );
    }
  }

  async connect() {
    try {
      Logger.info("Connecting to RabbitMQ...");
      const connection = await connect(this.settings.url);
      this.channel = await connection.createChannel();
      Logger.info("Successfully connected to RabbitMQ.");
    } catch (cause) {
      Logger.error("Unable to connect to RabbitMQ.");
      Logger.error(cause);
    }
  }

  async listen(queue: string, callback) {
    await this.channel.assertQueue(queue);
    await this.channel.consume(queue, (message: object) => callback(message));
  }
}
